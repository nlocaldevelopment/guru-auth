module Guru 
  module Auth 
    module Middlewares 
      class Authentication < Faraday::Middleware
        def initialize(app = nil, options = {})
          super(app)
          @options = options
          @vendor = options[:vendor] || ENV['SOURCE_VENDOR']
          @version = options[:version] || ENV['SOURCE_VERSION']
        end

        def call(env)
          ::Guru::Auth::Token.create  unless ::RequestStore[:token]
          ::Guru::Auth::Token.refresh unless ::RequestStore[:token]&.access_token

          env[:request_headers]["Accept"] = "application/json,application/vnd.#{@vendor}-v#{@version}"
          env[:request_headers]["Authorization"] = "#{::RequestStore.store[:token].token_type} " + ::RequestStore.store[:token].access_token

          @app.call(env).on_complete do |response_env|
            case response_env[:status]
            when 401
              ::RequestStore.store[:token].access_token= nil 
              raise ::Guru::Auth::Errors::Unauthorized, 'Invalid Credentials'
            when 406
              raise ::Guru::Auth::Errors::UnaceptableResponse, 'Accept header not supported'
            end
          end
        end
      end 
    end
  end
end

