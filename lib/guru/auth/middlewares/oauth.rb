module Guru 
  module Auth 
    module Middlewares 
      class Guru::Auth::Middlewares::Oauth < Faraday::Middleware
        def initialize(app = nil, options = {})
          super(app)
          @options = options
          @vendor = options[:vendor] || ENV['AUTH_VENDOR']
          @version = options[:version] || ENV['AUTH_VERSION'] 
        end

        def call(env)
          @app.call(env).on_complete do |response_env|
            env[:request_headers]["Accept"] = "application/json,application/vnd.#{@vendor}-#{@version}"
            case response_env[:status]
            when 401
              ::RequestStore.store[:token] = nil
              raise ::Guru::Auth::Errors::Unauthenticated, 'Need credentials'
            when 403
              ::RequestStore.store[:token] = nil 
              raise ::Guru::Auth::Errors::Unauthorized, 'Invalid Credentials'
            end
          end
        end
      end 
    end 
  end   
end
