module Guru 
  module Auth 
    class Token < ::Spyke::Base
      uri 'app/token'
      include_root_in_json false
   
      self.connection= ::Faraday.new(url: "#{ENV['AUTH_METHOD'] || 'https'}://#{ ENV['AUTH_HOST'] || 'localhost:3007' }") do |c|
        c.request   :json

        #Request
        c.use  ::Guru::Auth::Middlewares::Oauth, vendor: ENV['SOURCE_VENDOR'], version: ENV['SOURCE_VERSION']
     
        #Response
        c.use  ::Guru::Api::Middlewares::JsonParser

        c.adapter   :typhoeus
      end

      def self.create(**args)
        args={ adapter: ENV['AUTH_ADAPTER'],
              realm: ENV['ÀUTH_REALM'],
              provider: ENV['AUTH_PROVIDER'],
              credentials: {
                type: ENV['AUTH_TYPE'],
                scope: "#{ENV['AUTH_SCOPE']}",
                client_id: ENV['AUTH_ID'],
                client_secret: ENV['AUTH_SECRET']
              }
            }.update(args) 

        ::RequestStore[:token]= super(args)
      end

      def self.refresh
        result= self.request(:post, 
                     'refresh_token',
                     adapter: ENV['AUTH_ADAPTER'],
                     realm: ENV['AUTH_REALM'],
                     refresh_token: self.refresh_token,
                     credentials: {
                       client_id: ENV['AUTH_ID'],
                       client_secret: ENV['AUTH_SECRET']
                     }
                    )

        unless result&.body&.dig(:data).blank?            
          ::RequestStore[:token]= self.new(result.body[:data])
        else
          self.create 
        end
      end

      def self.access_token
        self.create if ::RequestStore[:token].blank?
        ::RequestStore.store[:token].access_token
      end 

      def self.refresh_token
        self.create if ::RequestStore[:token].blank?  
        ::RequestStore.store[:token].refresh_token
      end 

    end
  end 
end

