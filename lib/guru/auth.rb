require "guru/auth/version"
require "guru/api"
require "kaminari"
require "spyke"
require "spyke/paginate"
require "multi_json"
require "request_store"




module Guru
  module Auth
    class Error < StandardError; end
    # Your code goes here...
  end
end

Dir[File.expand_path("../auth/**/*.rb", __FILE__)].each{ |f| require f}
