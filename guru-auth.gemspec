# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require_relative 'lib/guru/auth/version'

Gem::Specification.new do |spec|
  spec.name          = "guru-auth"
  spec.version       = Guru::Auth::VERSION
  spec.authors       = ["Daniel Prado"]
  spec.email         = ["daniel.prado@publicar.com"]

  spec.summary       = %q{Client for Guru Auth services}
  spec.description   = %q{Client for Guru Auth services}
  spec.homepage      = "https://bitbucket.org/nlocaldevelopment/guru-auth.git"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata['allowed_push_host'] = "bitbucket.com"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/nlocaldevelopment/guru-auth.git"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/nlocaldevelopment/guru-auth.git"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_development_dependency "rake", ">= 12.0"
  spec.add_development_dependency "rspec", ">= 3.0"
  spec.add_runtime_dependency "spyke"
  spec.add_runtime_dependency "kaminari"
  spec.add_runtime_dependency "request_store"
  spec.add_runtime_dependency "guru-api"
  spec.add_runtime_dependency "spyke-paginate"
end
